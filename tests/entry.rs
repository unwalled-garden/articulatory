use std::sync::Arc;
use tonic::Request;

use ::articulatory::pb::articulatory_server::Articulatory;

#[tokio::test]
async fn test_create_and_read_entry() {
    let file_store = Arc::new(::articulatory::stores::file_store::FileStore::new(
        String::from("test-dir"),
    ));
    let articulatory = ::articulatory::Articulatory::new(file_store);
    let articulatory_service =
        articulatory::grpc_service::ArticulatoryGrpcService::new(articulatory);

    let create_request = Request::new(articulatory::pb::CreateEntryRequest {
        title: String::from("Nelly the Elephant"),
        contents: String::from("## Markdown title"),
        contents_format: articulatory::pb::EntryContentsFormat::Markdown as i32,
    });

    let create_response = articulatory_service.create_entry(create_request).await;

    assert!(create_response.is_ok());

    let create_response = create_response.unwrap().into_inner();

    assert!(create_response.entry_id.is_some());

    let read_request = Request::new(articulatory::pb::ReadEntryRequest {
        entry_id: create_response.entry_id.into(),
    });

    let read_response = articulatory_service.read_entry(read_request).await;

    assert!(read_response.is_ok());

    let read_response = read_response.unwrap().into_inner();
    assert!(read_response.title == "Nelly the Elephant");
}
