use std::io::Error;

use chrono::Utc;

use pulldown_cmark::{html, Options, Parser};
use uuid::Uuid;

#[derive(Copy, Clone, serde::Serialize, serde::Deserialize)]
pub enum ContentsFormat {
    Markdown,
}

pub struct Entry {
    pub title: String,
    pub contents_format: ContentsFormat,
    pub timestamp: chrono::DateTime<Utc>,
    pub uuid: uuid::Uuid,
    pub contents: String,
    pub html_contents: String,
}

impl Entry {
    pub fn new(
        title: String,
        contents: String,
        contents_format: ContentsFormat,
        timestamp: chrono::DateTime<Utc>,
    ) -> Self {
        let html_contents = generate_html_contents(&contents);
        let uuid = Uuid::new_v4();
        return Entry {
            title,
            contents_format,
            timestamp,
            uuid,

            contents,
            html_contents,
        };
    }
}

fn generate_html_contents(contents: &String) -> String {
    let mut options: Options = Options::empty();
    options.set(Options::ENABLE_STRIKETHROUGH, true);
    let parser = Parser::new_ext(contents.as_str(), options);
    let mut html_output: String = String::with_capacity(contents.len() * 3 / 2);
    html::push_html(&mut html_output, parser);
    html_output
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct EntryMetadataJson {
    pub title: String,
    pub contents_format: ContentsFormat,
    pub timestamp: i64,
    pub id: uuid::Uuid,
}

pub fn entry_metadata_to_json(entry: &Entry) -> Result<String, Error> {
    let entry_metadata = EntryMetadataJson {
        title: entry.title.clone(),
        contents_format: entry.contents_format,
        timestamp: entry.timestamp.timestamp(),
        id: entry.uuid,
    };

    serde_json::to_string(&entry_metadata).or_else(|serde_error| {
        Err(Error::new(
            std::io::ErrorKind::InvalidData,
            format!("Failed to convert to JSON: {}", serde_error),
        ))
    })
}

pub fn entry_metadata_from_json(json: &String) -> Result<EntryMetadataJson, Error> {
    serde_json::from_str(json).or_else(|serde_error| {
        Err(Error::new(
            std::io::ErrorKind::InvalidData,
            format!("Failed to convert from JSON: {}", serde_error),
        ))
    })
}
