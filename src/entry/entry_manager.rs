use super::entry_store::EntryStore;
use std::sync::Arc;

#[tonic::async_trait]
pub trait EntryManager: Send + Sync {
    async fn create_entry(
        &self,
        title: String,
        contents: String,
        contents_format: crate::entry::ContentsFormat,
        timestamp: chrono::NaiveDateTime,
    ) -> Result<uuid::Uuid, String>;
}

pub struct EntryManagerImpl<T: EntryStore> {
    pub store: Arc<T>,
}

impl<T: EntryStore> EntryManagerImpl<T> {
    pub fn new(store: &Arc<T>) -> Self {
        return EntryManagerImpl {
            store: Arc::clone(store),
        };
    }
}

#[tonic::async_trait]
impl<T: EntryStore> EntryManager for EntryManagerImpl<T> {
    async fn create_entry(
        &self,
        title: String,
        contents: String,
        contents_format: crate::entry::ContentsFormat,
        timestamp: chrono::NaiveDateTime,
    ) -> Result<uuid::Uuid, String> {
        let entry = crate::entry::Entry::new(title, contents, contents_format, timestamp);
        let result = self.store.store_entry(&entry).await;
        return result.and(Ok(entry.uuid));
    }
}
