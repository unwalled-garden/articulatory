use std::io::Error;

#[tonic::async_trait]
pub trait EntryStore: Send + Sync {
    async fn store_entry(&self, entry: &super::Entry) -> Result<(), Error>;
    async fn load_entry(&self, entry_id: uuid::Uuid) -> Result<super::Entry, Error>;
}
