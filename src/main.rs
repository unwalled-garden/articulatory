#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    articulatory::server::run().await
}
