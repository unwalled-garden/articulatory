use tonic::{Request, Response, Status};

pub struct UnwalledGrpcService {}

use url::Url;

#[tonic::async_trait]
impl crate::pb::unwalled_server::Unwalled for UnwalledGrpcService {
    async fn establish_connection(
        &self,
        request: Request<crate::pb::EstablishConnectionRequest>,
    ) -> Result<Response<crate::pb::EstablishConnectionResponse>, Status> {
        let establish_connection_request = request.into_inner();
        let url = match Url::parse(&establish_connection_request.origin_url) {
            Ok(url) => url,
            Err(_) => {
                return Err(Status::invalid_argument(format!(
                    "Invalid URL: {}",
                    establish_connection_request.origin_url
                )))
            }
        };
        let origin_url = String::from(url.as_str());

        let validation_token = establish_connection_request.validation_token;

        let mut client =
            match crate::pb::unwalled_client::UnwalledClient::connect("http://[::1]:50051").await {
                Ok(client) => client,
                Err(_) => {
                    return Err(Status::not_found(format!(
                        "Unable to connect to URL: {}",
                        origin_url
                    )))
                }
            };

        let validate_connection_request = crate::pb::ValidateConnectionRequest {
            origin_url: origin_url.clone(),
            validation_token,
        };

        match client
            .validate_connection(validate_connection_request)
            .await
        {
            Ok(_) => {}
            Err(_) => {
                return Err(Status::not_found(format!(
                    "Unable to validate connection request from URL: {}",
                    origin_url
                )))
            }
        };

        let response = crate::pb::EstablishConnectionResponse {};

        Ok(Response::new(response))
    }

    async fn validate_connection(
        &self,
        _request: Request<crate::pb::ValidateConnectionRequest>,
    ) -> Result<Response<crate::pb::ValidateConnectionResponse>, Status> {
        let response = crate::pb::ValidateConnectionResponse {};

        Ok(Response::new(response))
    }

    async fn accept_connection(
        &self,
        _request: Request<crate::pb::AcceptConnectionRequest>,
    ) -> Result<Response<crate::pb::AcceptConnectionResponse>, Status> {
        let response = crate::pb::AcceptConnectionResponse {};

        Ok(Response::new(response))
    }

    async fn retrieve_authentication_token(
        &self,
        _request: Request<crate::pb::RetrieveAuthenticationTokenRequest>,
    ) -> Result<Response<crate::pb::RetrieveAuthenticationTokenResponse>, Status> {
        let authentication_token = String::from("Hello");
        let response = crate::pb::RetrieveAuthenticationTokenResponse {
            authentication_token,
        };

        Ok(Response::new(response))
    }

    async fn report_reference(
        &self,
        _request: Request<crate::pb::ReportReferenceRequest>,
    ) -> Result<Response<crate::pb::ReportReferenceResponse>, Status> {
        let response = crate::pb::ReportReferenceResponse {};

        Ok(Response::new(response))
    }

    async fn advertise_content(
        &self,
        _request: Request<crate::pb::AdvertiseContentRequest>,
    ) -> Result<Response<crate::pb::AdvertiseContentResponse>, Status> {
        let response = crate::pb::AdvertiseContentResponse {};

        Ok(Response::new(response))
    }
}
