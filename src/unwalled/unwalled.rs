use crate::pb::{AcceptConnectionRequest, EstablishConnectionRequest};

use crate::pb::unwalled_client::UnwalledClient;
use tonic::transport::Channel;

use super::SqliteStore;
use super::{Connection, Direction, PendingConnection, UnwalledStore};

use thiserror::Error;
use rand::distributions::{Alphanumeric, DistString};
use rusty_paseto::prelude::*;

pub struct Unwalled {
    local_url: String,
    sqlite_store: SqliteStore,
    paseto_key: PasetoSymmetricKey::<V4, Local>,
}

#[derive(Error, Debug)]
pub enum UnwalledError {
    #[error("Unable to reach connection")]
    NetworkError(#[from] tonic::transport::Error),
    #[error("Error from connection")]
    ConnectionError(#[from] tonic::Status),
    #[error("Failed to read or write to database")]
    DatabaseError(#[from] sqlx::Error),
}

impl Unwalled {
    pub async fn new(
        local_url: String,
        unwalled_db_file: String,
	key_string: Option<String>,
    ) -> Result<Unwalled, UnwalledError> {
        let sqlite_store = SqliteStore::new(&unwalled_db_file).await?;

	let paseto_key = match key_string {
	    Some(key_string) => PasetoSymmetricKey::<V4, Local>::from(Key::from(key_string.as_bytes())),
	    None => Unwalled::generate_key(),
	};

        Ok(Unwalled {
            local_url,
            sqlite_store,
	    paseto_key
        })
    }

    async fn connect(url_str: String) -> Result<UnwalledClient<Channel>, tonic::transport::Error> {
        crate::pb::unwalled_client::UnwalledClient::connect(url_str.clone()).await
    }

    fn generate_key() -> PasetoSymmetricKey::<V4, Local> {
	let key_string = Alphanumeric.sample_string(&mut rand::thread_rng(), 32);
	 PasetoSymmetricKey::<V4, Local>::from(Key::from(key_string.as_bytes()))
    }

    pub async fn establish_connection(&self, url: &url::Url) -> Result<(), UnwalledError> {
        let url_str = String::from(url.as_str());

        let mut client = Unwalled::connect(url_str).await?;

        let establish_connection_request = EstablishConnectionRequest {
            origin_url: self.local_url.clone(),
            validation_token: String::from(""),
        };

        client
            .establish_connection(establish_connection_request)
            .await?;

        Ok(())
    }

    pub async fn accept_connection(
        self,
        pending_connection: &PendingConnection,
        connection_type: &str,
    ) -> Result<(), UnwalledError> {
        let mut client = Unwalled::connect(String::from(pending_connection.url.as_str())).await?;

        let accept_connection_request = AcceptConnectionRequest {
            origin_url: self.local_url,
            validation_token: String::from(""),
        };

        client.accept_connection(accept_connection_request).await?;

        let existing_connection = self
            .sqlite_store
            .load_connection(pending_connection.url.as_str())
            .await?;

        let new_direction = existing_connection.map_or(Direction::Outbound, |c| c.direction | Direction::Outbound);

        let connection = Connection {
            url: pending_connection.url.clone(),
            connection_type: String::from(connection_type),
            direction: new_direction,
        };

        self.sqlite_store.store_connection(&connection).await?;

        Ok(())
    }

    pub async fn receive_establish_connection(self) -> Result<(), std::io::Error> {
        Ok(())
    }
}
