use crate::unwalled::connection::Connection;

#[tonic::async_trait]
pub trait UnwalledStore: Send + Sync {
    //async fn store_pending_connection(&self, pending_connection: &super::PendingConnection) -> Result<(), std::io::Error>;

    async fn store_connection(&self, connection: &super::Connection) -> Result<(), sqlx::Error>;

    async fn load_connection(&self, url: &str) -> Result<Option<super::Connection>, sqlx::Error>;

    async fn load_connections_by_type(
        &self,
        connection_type: &str,
    ) -> Result<std::vec::Vec<Connection>, sqlx::Error>;

    async fn store_key(&self, name: &str, key: &str) -> Result<(), sqlx::Error>;

    async fn load_key(&self, name: &str) -> Result<Option<String>, sqlx::Error>;
}
