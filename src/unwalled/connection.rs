#[derive(sqlx::Type, Debug, PartialEq, Eq)]
#[sqlx(type_name = "direction", rename_all = "UPPERCASE")]
pub enum Direction {
    None = 0,
    Inbound = 2,
    Outbound = 4,
    Bidirectional = 6,
}

impl From<i32> for Direction {
    fn from(value: i32) -> Direction {
        match value {
            2 => Direction::Inbound,
            4 => Direction::Outbound,
            6 => Direction::Bidirectional,
            _ => Direction::None,
        }
    }
}

impl std::ops::BitOr<Direction> for Direction {
    type Output = Direction;

    fn bitor(self, rhs: Direction) -> Direction {
        Direction::from((self as i32) | (rhs as i32))
    }
}

impl std::ops::BitXor<Direction> for Direction {
    type Output = Direction;

    fn bitxor(self, rhs: Direction) -> Direction {
        Direction::from((self as i32) ^ (rhs as i32))
    }
}

pub struct PendingConnection {
    pub url: String,
    pub pending_timestamp: chrono::DateTime<chrono::Utc>,
    pub direction: Direction,
}

pub struct ConnectionType {
    pub connection_type: String,
    pub description: String,
}

pub struct Connection {
    pub url: String,
    pub connection_type: String,
    pub direction: Direction,
}
