use std::convert::From;

pub struct SqliteStore {
    sqlite_file: String,
    pool: sqlx::sqlite::SqlitePool,
}

impl SqliteStore {
    pub async fn new(sqlite_file: &String) -> Result<Self, sqlx::Error> {
        let pool = sqlx::sqlite::SqlitePoolOptions::new()
            .connect(sqlite_file)
            .await?;

        return Ok(Self {
            sqlite_file: sqlite_file.clone(),
            pool,
        });
    }
}


impl From<&str> for super::Direction {
    fn from(direction: &str) -> Self {
        match direction {
            "INBOUND" => super::Direction::Inbound,
            "OUTBOUND" => super::Direction::Outbound,
            "BIDIRECTIONAL" => super::Direction::Bidirectional,
            _ => super::Direction::None, // This is a safe fallback, no sharing
        }
    }
}

#[tonic::async_trait]
impl super::unwalled_store::UnwalledStore for SqliteStore {
    async fn store_connection(&self, connection: &super::Connection) -> Result<(), sqlx::Error> {
        match sqlx::query!(
            r#"
INSERT INTO connections ( url, connection_type, direction )
VALUES ( ?1, ?2, ?3 )
ON CONFLICT(url) DO
UPDATE SET connection_type = ?2, direction = ?3;"#,
            connection.url,
            connection.connection_type,
            connection.direction
        )
        .execute(&self.pool)
        .await
        {
            Ok(_) => Ok(()),
            Err(e) => Err(e),
        }
    }

    async fn load_connection(&self, url: &str) -> Result<Option<super::Connection>, sqlx::Error> {
        sqlx::query_as!(
            super::Connection,
            r#"SELECT url, connection_type, direction AS "direction: super::Direction" FROM connections WHERE url = ?1"#,
            url
        )
        .fetch_optional(&self.pool)
        .await
    }

    async fn load_connections_by_type(
        &self,
        connection_type: &str,
    ) -> Result<std::vec::Vec<super::Connection>, sqlx::Error> {
        sqlx::query_as!(
            super::Connection,
            r#"SELECT url, connection_type, direction AS "direction: super::Direction" FROM connections WHERE connection_type = ?1"#,
            connection_type
        )
        .fetch_all(&self.pool)
        .await
    }


    async fn store_key(&self, name: &str, key: &str) -> Result<(), sqlx::Error> {
        match sqlx::query!(
            r#"
INSERT INTO keys ( name, key )
VALUES ( ?1, ?2 )
ON CONFLICT(name) DO
UPDATE SET key = ?2;"#,
            name,
	    key
        )
        .execute(&self.pool)
        .await
        {
            Ok(_) => Ok(()),
            Err(e) => Err(e),
        }

    }


    async fn load_key(&self, name: &str) -> Result<Option<String>, sqlx::Error> {
        sqlx::query_scalar!(
            r#"SELECT key FROM keys WHERE name = ?1"#,
            name
        )
        .fetch_optional(&self.pool)
        .await
    }

}

#[cfg(test)]
mod tests {
    use super::super::{Connection, Direction, UnwalledStore};
    use super::*;

    static URL: &str = "unwalled::some-domain.org";

    #[sqlx::test]
    async fn test_store_connection(pool: sqlx::Pool<sqlx::Sqlite>) -> sqlx::Result<()> {
        let store = SqliteStore {
            sqlite_file: String::from(""),
            pool,
        };

        let url_string = String::from(URL);
        let connection = Connection {
            url: url_string,
            connection_type: String::from("anonymous"),
            direction: Direction::Outbound,
        };

        let store_result = store.store_connection(&connection).await;
        assert!(store_result.is_ok());

        Ok(())
    }

    #[sqlx::test]
    async fn test_load_connection(pool: sqlx::Pool<sqlx::Sqlite>) -> sqlx::Result<()> {
        let store = SqliteStore {
            sqlite_file: String::from(""),
            pool,
        };

        let url_string = String::from(URL);
        let store_connection = Connection {
            url: url_string,
            connection_type: String::from("anonymous"),
            direction: Direction::Inbound,
        };

        let store_result = store.store_connection(&store_connection).await;
        assert!(store_result.is_ok());

        let load_result = store.load_connection(URL).await;
        assert!(load_result.is_ok());
        let load_connection = load_result.unwrap();
        assert_eq!(store_connection.url, load_connection.url);
        assert_eq!(
            store_connection.connection_type,
            load_connection.connection_type
        );
        assert_eq!(store_connection.direction, load_connection.direction);

        Ok(())
    }

    #[sqlx::test]
    async fn test_load_connections_by_type(pool: sqlx::Pool<sqlx::Sqlite>) -> sqlx::Result<()> {
        let store = SqliteStore {
            sqlite_file: String::from(""),
            pool,
        };

        let url_string = String::from(URL);
        let store_connection = Connection {
            url: url_string,
            connection_type: String::from("anonymous"),
            direction: Direction::Bidirectional,
        };

        let store_result = store.store_connection(&store_connection).await;
        assert!(store_result.is_ok());

        let load_result = store
            .load_connections_by_type(String::from("anonymous").as_str())
            .await;
        assert!(load_result.is_ok());
        let load_connections = load_result.unwrap();
        assert_eq!(store_connection.url, load_connections[0].url);
        assert_eq!(
            store_connection.connection_type,
            load_connections[0].connection_type
        );

        Ok(())
    }
}
