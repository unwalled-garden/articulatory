use axum::{
    http::{StatusCode, Uri},
    routing::get,
};
use std::sync::Arc;

pub async fn run() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse().unwrap();

    let git_store = Arc::new(crate::stores::file_store::FileStore::new(String::from(
        "file-store",
    )));
    let articulatory = crate::Articulatory::new(git_store);
    let articulatory_grpc_service = crate::grpc_service::ArticulatoryGrpcService::new(articulatory);
    let articulatory_grpc_server = tonic_web::enable(
        crate::pb::articulatory_server::ArticulatoryServer::new(articulatory_grpc_service),
    );

    let unwalled_grpc_service = crate::unwalled::unwalled_grpc_service::UnwalledGrpcService {};
    let unwalled_grpc_server = tonic_web::enable(crate::pb::unwalled_server::UnwalledServer::new(
        unwalled_grpc_service,
    ));

    let reflection_server = tonic_web::enable(
        tonic_reflection::server::Builder::configure()
            .register_encoded_file_descriptor_set(crate::pb::ARTICULATORY_FILE_DESCRIPTOR_SET)
            .register_encoded_file_descriptor_set(crate::pb::UNWALLED_FILE_DESCRIPTOR_SET)
            .build()
            .unwrap(),
    );

    let reflection_server_alpha = tonic_web::enable(
        tonic_reflection::server::Builder::configure()
            .register_encoded_file_descriptor_set(crate::pb::ARTICULATORY_FILE_DESCRIPTOR_SET)
            .register_encoded_file_descriptor_set(crate::pb::UNWALLED_FILE_DESCRIPTOR_SET)
            .build()
            .unwrap(),
    );

    let web_router = axum::Router::new()
        .route(
            "/articulatory.Articulatory/*rpc",
            axum::routing::any_service(articulatory_grpc_server),
        )
        .route(
            "/unwalled.Unwalled/*rpc",
            axum::routing::any_service(unwalled_grpc_server),
        )
        .route(
            "/grpc.reflection.v1.ServerReflection/*rpc",
            axum::routing::any_service(reflection_server),
        )
        .route(
            "/grpc.reflection.v1alpha.ServerReflection/*rpc",
            axum::routing::any_service(reflection_server_alpha),
        )
        .route("/", get(web_root))
        .fallback(fallback);

    println!("Articulatory listening on {}", addr);

    axum::Server::bind(&addr)
        .serve(web_router.into_make_service())
        .await?;

    Ok(())
}

async fn web_root() -> &'static str {
    "Hello World!"
}

async fn fallback(uri: Uri) -> (StatusCode, &'static str) {
    println!("Unhandled path {}", uri);
    (StatusCode::NOT_FOUND, "Not Found")
}
