use entry::Entry;

pub mod pb {
    tonic::include_proto!("articulatory");
    tonic::include_proto!("unwalled");

    pub(crate) const ARTICULATORY_FILE_DESCRIPTOR_SET: &[u8] =
        tonic::include_file_descriptor_set!("articulatory_descriptor");

    pub(crate) const UNWALLED_FILE_DESCRIPTOR_SET: &[u8] =
        tonic::include_file_descriptor_set!("unwalled_descriptor");
}

pub mod entry;
pub mod grpc_service;
pub mod server;
pub mod stores;
pub mod unwalled;

#[derive(Clone)]
pub struct Articulatory<T: entry::EntryStore> {
    pub entry_store: std::sync::Arc<T>,
}

impl<T: entry::EntryStore> Articulatory<T> {
    pub fn new(entry_store: std::sync::Arc<T>) -> Self {
        Articulatory {
            entry_store: entry_store.clone(),
        }
    }

    pub async fn create_entry(
        &self,
        title: String,
        contents: String,
        contents_format: crate::entry::ContentsFormat,
    ) -> Result<uuid::Uuid, std::io::Error> {
        let timestamp = chrono::offset::Utc::now();
        let entry = crate::entry::Entry::new(title, contents, contents_format, timestamp);
        let result = self.entry_store.store_entry(&entry).await;
        return result.and(Ok(entry.uuid));
    }

    pub async fn read_entry(&self, entry_id: uuid::Uuid) -> Result<Entry, std::io::Error> {
        let result = self.entry_store.load_entry(entry_id).await;
        return result;
    }
}
