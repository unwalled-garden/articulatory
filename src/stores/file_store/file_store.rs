use chrono::DateTime;

use std::io::Error;
use std::io::ErrorKind;

use tokio::fs::DirBuilder;
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWriteExt;

use crate::entry::entry;

pub struct FileStore {
    file_store_dir: String,
}

impl FileStore {
    pub fn new(file_store_dir: String) -> Self {
        Self { file_store_dir }
    }
}

fn compose_filename(dirname: &String, filename: &String) -> String {
    format!("{}/{}", dirname, filename)
}

async fn write_text_file(filename: &String, text: &String) -> Result<(), Error> {
    let mut file = File::create(filename).await?;

    file.write_all(text.as_bytes()).await
}

async fn read_text_file(filename: &String) -> Result<String, Error> {
    let mut file = File::open(filename).await?;

    let mut contents = vec![];
    file.read_to_end(&mut contents).await?;

    String::from_utf8(contents).or(Err(Error::new(
        ErrorKind::Other,
        "Failed to convert to utf8",
    )))
}

struct FileNames {
    metadata: String,
    contents: String,
    html_contents: String,
}

fn compose_filenames(file_store_dir: &String, entry_id: uuid::Uuid) -> FileNames {
    let dirname = format!("{}/entries/{}", file_store_dir, entry_id);
    FileNames {
        metadata: compose_filename(&dirname, &String::from("metadata.json")),
        contents: compose_filename(&dirname, &String::from("contents.md")),
        html_contents: compose_filename(&dirname, &String::from("html_contents.html")),
    }
}

#[tonic::async_trait]
impl crate::entry::EntryStore for FileStore {
    async fn store_entry(&self, entry: &crate::entry::Entry) -> Result<(), Error> {
        let dirname = format!("{}/entries/{}", self.file_store_dir, entry.uuid);

        let mut builder = DirBuilder::new();
        builder.recursive(true);
        builder.create(dirname.clone()).await?;

        let json_metadata = entry::entry_metadata_to_json(entry)?;

        let filenames = compose_filenames(&self.file_store_dir, entry.uuid);

        let metadata_write = write_text_file(&filenames.metadata, &json_metadata);
        let contents_write = write_text_file(&filenames.contents, &entry.contents);
        let html_contents_write = write_text_file(&filenames.html_contents, &entry.html_contents);

        let (metadata_result, contents_result, html_contents_result) =
            tokio::join!(metadata_write, contents_write, html_contents_write);

        [metadata_result, contents_result, html_contents_result]
            .into_iter()
            .collect()
    }

    async fn load_entry(&self, entry_id: uuid::Uuid) -> Result<crate::entry::Entry, Error> {
        let filenames = compose_filenames(&self.file_store_dir, entry_id);

        let metadata_read = read_text_file(&filenames.metadata);
        let contents_read = read_text_file(&filenames.contents);
        let html_contents_read = read_text_file(&filenames.html_contents);

        let metadata_string = metadata_read.await?;
        let metadata = entry::entry_metadata_from_json(&metadata_string)?;
        let timestamp = DateTime::from_timestamp(metadata.timestamp, 0)
            .ok_or_else(|| Error::new(ErrorKind::InvalidData, "Invalid timestamp"))?;

        Ok(entry::Entry {
            title: metadata.title,
            uuid: metadata.id,
            timestamp,
            contents_format: metadata.contents_format,
            html_contents: html_contents_read.await?,
            contents: contents_read.await?,
        })
    }
}
