use std::io::ErrorKind;

#[derive(Default)]
pub struct GitStore {}

#[tonic::async_trait]
impl crate::entry::EntryStore for GitStore {
    async fn store_entry(&self, _entry: &crate::entry::Entry) -> Result<(), std::io::Error> {
        return Ok(());
    }

    async fn load_entry(
        &self,
        _entry_id: uuid::Uuid,
    ) -> Result<crate::entry::Entry, std::io::Error> {
        Err(std::io::Error::new(ErrorKind::Other, "Not implemented"))
    }
}
