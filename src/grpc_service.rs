use tonic::{Request, Response, Status};

use crate::{entry::EntryStore, pb::ReadEntryResponse, Articulatory};

#[derive(Clone)]
pub struct ArticulatoryGrpcService<T: EntryStore> {
    articulatory: Articulatory<T>,
}

impl<T: EntryStore> ArticulatoryGrpcService<T> {
    pub fn new(articulatory: Articulatory<T>) -> Self {
        ArticulatoryGrpcService { articulatory }
    }
}

fn decode_uuid(uuid: crate::pb::Uuid) -> Result<uuid::Uuid, Status> {
    match uuid::Uuid::parse_str(&uuid.id) {
        Ok(id) => Result::Ok(id),
        Err(_) => Result::Err(Status::invalid_argument("Failed to decode UUID")),
    }
}

fn decode_optional_uuid(
    optional_uuid: Option<crate::pb::Uuid>,
    field_name: &str,
) -> Result<uuid::Uuid, Status> {
    match optional_uuid {
        Some(id) => decode_uuid(id),
        None => Err(Status::invalid_argument(format!(
            "Missing field {}",
            field_name
        ))),
    }
}

fn decode_contents_format(
    contents_format: crate::pb::EntryContentsFormat,
) -> crate::entry::ContentsFormat {
    match contents_format {
        crate::pb::EntryContentsFormat::Markdown => crate::entry::ContentsFormat::Markdown,
    }
}

fn encode_contents_format(
    contents_format: crate::entry::ContentsFormat,
) -> crate::pb::EntryContentsFormat {
    match contents_format {
        crate::entry::ContentsFormat::Markdown => crate::pb::EntryContentsFormat::Markdown,
    }
}

#[tonic::async_trait]
impl<T: EntryStore + 'static> crate::pb::articulatory_server::Articulatory
    for ArticulatoryGrpcService<T>
{
    async fn create_entry(
        &self,
        request: Request<crate::pb::CreateEntryRequest>,
    ) -> Result<Response<crate::pb::CreateEntryResponse>, Status> {
        println!("Got a request from {:?}", request.remote_addr());

        let proto = request.into_inner();

        let pb_contents_format = crate::pb::EntryContentsFormat::try_from(proto.contents_format)
            .or_else(|_| {
                Err(Status::invalid_argument(format!(
                    "Unknown contents format: {}",
                    proto.contents_format
                )))
            })?;

        let contents_format = decode_contents_format(pb_contents_format);

        let entry_id = self
            .articulatory
            .create_entry(proto.title, proto.contents, contents_format)
            .await
            .or_else(|err| Err(Status::internal(format!("Failed to create entry: {}", err))))?;

        let entry_uuid = crate::pb::Uuid {
            id: entry_id.to_string(),
        };
        let reply = crate::pb::CreateEntryResponse {
            entry_id: Some(entry_uuid),
        };
        return Ok(Response::new(reply));
    }

    async fn read_entry(
        &self,
        request: Request<crate::pb::ReadEntryRequest>,
    ) -> Result<Response<crate::pb::ReadEntryResponse>, Status> {
        let proto = request.into_inner();

        let entry_id = decode_optional_uuid(proto.entry_id, "entry_id")?;

        let entry = self
            .articulatory
            .read_entry(entry_id)
            .await
            .or_else(|err| Err(Status::internal(format!("Failed to read entry: {}", err))))?;

        let contents_format = encode_contents_format(entry.contents_format) as i32;

        Ok(Response::new(ReadEntryResponse {
            title: entry.title,
            timestamp: Some(prost_types::Timestamp {
                seconds: entry.timestamp.timestamp(),
                nanos: 0,
            }),
            contents_format,
            contents: entry.contents,
            html_contents: entry.html_contents,
        }))
    }
}
