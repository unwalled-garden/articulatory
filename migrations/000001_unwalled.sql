CREATE TABLE IF NOT EXISTS connections
(
	url		TEXT PRIMARY KEY NOT NULL,
    	connection_type	TEXT NOT NULL,
	direction CHECK( direction IN ('INBOUND','OUTBOUND','BIDIRECTIONAL') ) NOT NULL,
	FOREIGN KEY(connection_type) REFERENCES connection_types(connection_type)
);

CREATE TABLE IF NOT EXISTS connection_types
(
	connection_type	TEXT PRIMARY KEY NOT NULL,
	description	TEXT
);

INSERT INTO connection_types (connection_type, description) VALUES ('anonymous', 'An anonymous connection');

CREATE TABLE IF NOT EXISTS pending_connections
(
	url			TEXT PRIMARY KEY NOT NULL,
	pending_timestamp	TEXT NOT NULL,
	direction CHECK( direction IN ('INBOUND','OUTBOUND','BIDIRECTIONAL') ) NOT NULL
);

CREATE TABLE IF NOT EXISTS keys
(
	name			TEXT PRIMARY KEY NOT NULL,
	key			TEXT NOT NULL
);
