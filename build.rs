use std::{env, path::PathBuf};

fn main() {
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());
    tonic_build::configure()
        .file_descriptor_set_path(out_dir.join("articulatory_descriptor.bin"))
        .compile(
            &["proto/articulatory/articulatory.proto"],
            &["proto/articulatory"],
        )
        .unwrap();

    tonic_build::configure()
        .file_descriptor_set_path(out_dir.join("unwalled_descriptor.bin"))
        .compile(&["proto/unwalled/unwalled.proto"], &["proto/unwalled"])
        .unwrap();
}
